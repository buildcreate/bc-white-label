<div class="bcwl-settings-template">
	<h1>BC White Label Settings</h1>
	<hr/>

	<span class="description">Settings coming soon!</span>

<?php if(false) : ?>
	<form method="post" action="">
		<p>
			Select Header Menu Items
			<?php 
				// build nested array for checkbox list display
				$admin_nodes = array();
				if($this->bcwl_admin_nodes){

					// get parent nodes
					foreach($this->bcwl_admin_nodes as $admin_node){
						if(!$admin_node->parent){
							$admin_nodes[$admin_node->id] = array('children' => array());
						}
					}				

					// add children to parent nodes
					foreach($this->bcwl_admin_nodes as $admin_node){
						if($admin_node->parent){
							$admin_nodes[$admin_node->parent]['children'][] = $admin_node->id;
						}
					}
				}
			?>

			<?php $bcwl_admin_nodes = get_option('bcwl_admin_nodes'); ?>
			<ul class="bcwl-admin-node-list">
			<?php foreach($admin_nodes as $k => $node) : ?>
				<li>
					<label>
						<input type="checkbox" name="bcwl_admin_nodes[]" value="<?php echo $k; ?>" <?php if(is_array($bcwl_admin_nodes) && in_array($k, $bcwl_admin_nodes)){echo 'checked="checked"';} ?>/> <?php echo $k; ?> 
					</label>
					<?php if($node['children']) : ?>
					<ul>
						<?php foreach($node['children'] as $child) : ?>
						<li>
							<label>
								<input type="checkbox" name="bcwl_admin_nodes[]" value="<?php echo $child; ?>" <?php if(is_array($bcwl_admin_nodes) && in_array($child, $bcwl_admin_nodes)){echo 'checked="checked"';} ?>/> <?php echo $child; ?> 
							</label>
						</li>
						<?php endforeach; ?>
					</ul>
					<?php endif; ?>
				</li>
			<?php endforeach; ?>
			</ul>
		</p>	
		<p>
			Select Sidebar Menu Items

			<?php global $submenu, $menu; ?>
			<pre>
			<?php var_dump($menu); ?>
			</pre>
		</p>
		<p>
			<input class="button-primary" type="submit" name="bcwl_save_settings" value="Save Settings" />
		</p>
	</form>
<?php endif; ?>
</div>