<?php
	class BC_WL_Settings {
		public $bcwl_admin_nodes = array();

		function __construct(){
			add_action('admin_menu', array($this, 'bcwl_settings'));
			add_action('admin_bar_menu', array($this, 'bcwl_get_all_toolbar_nodes'), 999);	
			add_action('admin_enqueue_scripts', array($this, 'bcwl_settings_styles'));	

			// check for posted settings
			if(isset($_POST['bcwl_save_settings'])){

				// save admin nodes
				$admin_nodes = $_POST['bcwl_admin_nodes'];
				update_option('bcwl_admin_nodes', $admin_nodes);
			}
		}

		// add submenu page
		function bcwl_settings(){
			add_submenu_page('options-general.php', 'BC White Label', 'BC White Label', 'manage_options', 'bcwl-settings', array($this, 'bcwl_settings_page'));
		}

		// submenu page template
		function bcwl_settings_page(){
			include(plugin_dir_path(__FILE__).'templates/settings-template.php');
		}

		// get nodes on load for later
		function bcwl_get_all_toolbar_nodes($wp_admin_bar){
			$this->bcwl_admin_nodes = $wp_admin_bar->get_nodes();
		}

		// admin settings styles
		function bcwl_settings_styles(){
        	wp_enqueue_style('bcwl-settings-css', plugins_url('../css/settings.css', __FILE__));
		}
	}

?>