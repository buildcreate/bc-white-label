<?php
	
	class BC_Login_Whitelabel {
		 
		function __construct() {
			add_action('login_enqueue_scripts', array($this, 'bcwl_login_css'));
			add_filter('login_headerurl', array($this, 'bcwl_login_logo_url'));
		}
		 
		function bcwl_login_css() {
		?>
		    <style type="text/css">
		    <?php if($logo = get_field('logo', 'option')) : ?>
		        #login h1 a, .login h1 a {
		            background-image: url(<?php echo $logo; ?>);
		            background-size:contain;
		            height:40px;
		            width:100%;
		        }
		    <?php endif; ?>
		    </style>
		<?php 
		}

		function bcwl_login_logo_url() {
		    return home_url();
		}
	}
?>